package ru.t1.shevyreva.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException(String arg) {
        super("Error! " + arg + " is not supported! Enter -h for list arguments.");
    }

}
