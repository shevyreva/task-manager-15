package ru.t1.shevyreva.tm.exception.entity;

public final class StatusNotFoundException extends AbstractEntityException {

    public StatusNotFoundException() {
        super("Status not found! ");
    }

}
