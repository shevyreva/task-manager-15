package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.controller.ICommandController;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.constant.ApplicationConst;
import ru.t1.shevyreva.tm.model.Command;
import ru.t1.shevyreva.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showWelcome() {
        System.out.println("**Welcome to Task Manager**");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", ApplicationConst.APP_MAJOR_VERSION, ApplicationConst.APP_MINOR_VERSION, ApplicationConst.APP_FIXES_VERSION);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used memory: " + usedMemoryFormat);
    }

    @Override
    public void showCommand() {
        System.out.println("[COMMAND]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArgument() {
        System.out.println("[ARGUMENT]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public void exitError() {
        System.exit(1);
    }

}
