package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.controller.IProjectController;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
    }

    public void showRemoveById() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        projectTaskService.removeByProjectId(project.getId());
    }

    public void showRemoveByIndex() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeByProjectId(project.getId());
    }

    public void showUpdateById() {
        System.out.println("[UPDATE BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    public void showUpdateByIndex() {
        System.out.println("[UPDATE BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    public void showByIndex() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    public void showById() {
        System.out.println("[SHOW BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    private void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[PROJECT ID]: " + project.getId());
        System.out.println("[PROJECT NAME]: " + project.getName());
        System.out.println("[PROJECT DESCRIPTION]: " + project.getDescription());
        System.out.println("[PROJECT STATUS]: " + Status.toName(project.getStatus()));
    }

    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusById(id, status);
    }

    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
    }

    public void startProjectStatusByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    public void startProjectStatusById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    public void completeProjectStatusByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    public void completeProjectStatusById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

}
