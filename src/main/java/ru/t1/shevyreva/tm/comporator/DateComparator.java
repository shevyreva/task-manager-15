package ru.t1.shevyreva.tm.comporator;

import ru.t1.shevyreva.tm.api.model.IHaveCreated;

import java.util.Comparator;

public enum DateComparator implements Comparator<IHaveCreated> {

    INSTANCE;

    @Override
    public int compare(final IHaveCreated o1, final IHaveCreated o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getCreated() == null || o2.getCreated() == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
