package ru.t1.shevyreva.tm.model;

import ru.t1.shevyreva.tm.api.model.IWBS;
import ru.t1.shevyreva.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NON_STARTED;

    private String projectId;

    private Date created = new Date();

    public Task() {

    }

    public Task(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return name + ": " + id + ";";
    }

    @Override
    public Date getCreated() {
        return created;
    }

    public void setDate(Date created) {
        this.created = created;
    }

}
