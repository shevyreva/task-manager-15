package ru.t1.shevyreva.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showById();

    void showByIndex();

    void showUpdateById();

    void showUpdateByIndex();

    void showRemoveById();

    void showRemoveByIndex();

    void completeProjectStatusById();

    void completeProjectStatusByIndex();

    void startProjectStatusById();

    void startProjectStatusByIndex();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

}
