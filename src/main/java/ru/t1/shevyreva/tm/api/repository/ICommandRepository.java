package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
