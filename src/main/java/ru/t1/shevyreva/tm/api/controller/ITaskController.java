package ru.t1.shevyreva.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

    void showById();

    void showByIndex();

    void showUpdateById();

    void showUpdateByIndex();

    void showRemoveById();

    void showRemoveByIndex();

    void showTasksByProjectId();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void completeTaskStatusById();

    void completeTaskStatusByIndex();

    void startTaskStatusById();

    void startTaskStatusByIndex();

}
