package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project findOneById(String Id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    void removeById(String Id);

    void removeByIndex(Integer index);

    boolean existsById(String id);

    Integer getSize();

}
