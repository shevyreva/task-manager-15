package ru.t1.shevyreva.tm.component;

import ru.t1.shevyreva.tm.api.controller.ICommandController;
import ru.t1.shevyreva.tm.api.controller.IProjectController;
import ru.t1.shevyreva.tm.api.controller.IProjectTaskController;
import ru.t1.shevyreva.tm.api.controller.ITaskController;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.controller.CommandController;
import ru.t1.shevyreva.tm.controller.ProjectController;
import ru.t1.shevyreva.tm.controller.ProjectTaskController;
import ru.t1.shevyreva.tm.controller.TaskController;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.service.CommandService;
import ru.t1.shevyreva.tm.service.ProjectService;
import ru.t1.shevyreva.tm.service.ProjectTaskService;
import ru.t1.shevyreva.tm.service.TaskService;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    public void start(final String[] args) {
        processArguments(args);
        initDemoData();
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                processCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    private void initDemoData() {
        projectService.add(new Project("B PROJECT", Status.COMPLETED));
        projectService.add(new Project("A PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("D PROJECT", Status.NON_STARTED));
        projectService.add(new Project("C PROJECT", Status.IN_PROGRESS));

        taskService.add(new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(new Task("SUPER TASK", Status.NON_STARTED));
    }

    public void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        exit();
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command.toLowerCase()) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommand();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArgument();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.showRemoveById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.showRemoveByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.showUpdateById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.showUpdateByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectStatusById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_UPDATE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectStatusById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectStatusByIndex();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.showRemoveById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.showRemoveByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.showUpdateById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.showUpdateByIndex();
                break;
            case CommandConst.TASK_UPDATE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_UPDATE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskStatusById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskStatusByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskStatusById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskStatusByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskToProject();
                break;
            case CommandConst.TASK_SHOW_ALL_BY_PROJECT_ID:
                taskController.showTasksByProjectId();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommand();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArgument();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    public void exit() {
        System.exit(0);
    }

}
